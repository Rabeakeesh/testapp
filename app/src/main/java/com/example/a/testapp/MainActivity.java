package com.example.a.testapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Boolean isRecordMode=true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button btn1,btn2,btn3,btn4,reset,save;
        btn1=findViewById(R.id.btn1);
        btn2=findViewById(R.id.btn2);
        btn3=findViewById(R.id.btn3);
        btn4=findViewById(R.id.btn4);
        reset=findViewById(R.id.reset);
        save=findViewById(R.id.save);
        final ArrayList<Integer> arrayList = new ArrayList<>();


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"you click Do",Toast.LENGTH_LONG).show();
                MediaPlayer mediaPlayer;
                if(isRecordMode){
                    arrayList.add(1);
                }else {

                }

                mediaPlayer=MediaPlayer.create(MainActivity.this,R.raw.btn4);
                mediaPlayer.start();
                btn1.setBackgroundColor(Color.YELLOW);
                if(mediaPlayer.isPlaying()){
                    btn2.setEnabled(false);
                    btn3.setEnabled(false);
                    btn4.setEnabled(false);

                }
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        btn1.setBackgroundResource(android.R.drawable.btn_default);
                        btn2.setEnabled(true);
                        btn3.setEnabled(true);
                        btn4.setEnabled(true);

                    }
                });

            }
        });


        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"you click Re",Toast.LENGTH_LONG).show();

                MediaPlayer mediaPlayer;
                if(isRecordMode){
                    arrayList.add(2);
                }else {

                }
                mediaPlayer=MediaPlayer.create(MainActivity.this,R.raw.btn14);
                mediaPlayer.start();
                btn2.setBackgroundColor(Color.BLUE);
                if(mediaPlayer.isPlaying()){
                    btn1.setEnabled(false);
                    btn3.setEnabled(false);
                    btn4.setEnabled(false);
                }
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        btn2.setBackgroundResource(android.R.drawable.btn_default);
                        btn3.setEnabled(true);
                        btn4.setEnabled(true);
                        btn1.setEnabled(true);

                    }
                });

            }
        });


        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"you click Me",Toast.LENGTH_LONG).show();

                MediaPlayer mediaPlayer;
                if(isRecordMode){
                    arrayList.add(3);
                }else {

                }
                mediaPlayer=MediaPlayer.create(MainActivity.this,R.raw.btn15);
                mediaPlayer.start();
                btn3.setBackgroundColor(Color.RED);
                if(mediaPlayer.isPlaying()){
                    btn1.setEnabled(false);
                    btn2.setEnabled(false);
                    btn4.setEnabled(false);
                }
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        btn3.setBackgroundResource(android.R.drawable.btn_default);
                        btn1.setEnabled(true);
                        btn2.setEnabled(true);
                        btn4.setEnabled(true);
                    }
                });

            }
        });


        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"you click Fa",Toast.LENGTH_LONG).show();

                MediaPlayer mediaPlayer;
                if(isRecordMode){
                    arrayList.add(4);
                }else {

                }
                mediaPlayer=MediaPlayer.create(MainActivity.this,R.raw.btn18);
                mediaPlayer.start();
                btn4.setBackgroundColor(Color.GREEN);
                if(mediaPlayer.isPlaying()){
                    btn1.setEnabled(false);
                    btn2.setEnabled(false);
                    btn3.setEnabled(false);
                }
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        btn4.setBackgroundResource(android.R.drawable.btn_default);
                        btn1.setEnabled(true);
                        btn2.setEnabled(true);
                        btn3.setEnabled(true);
                    }
                });

            }
        });


        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayList.clear();


            }
        });

        final SharedPreferences prefs = this.getSharedPreferences(
                "testapp", Context.MODE_PRIVATE);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mysave = new Intent(MainActivity.this,Second.class);
                startActivity(mysave);
                //To edit shared preferences use editor as follows
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("key","value");
                editor.commit();
            }
        });

    }

}

